class AddAttributesTodevise < ActiveRecord::Migration
  def change
    add_column    :profiles, :type, :string
    add_column    :profiles, :first_name, :string
    add_column    :profiles, :last_name, :string
    add_column    :profiles, :phone, :string
    add_column    :profiles, :gender, :string
    add_column    :profiles, :dob, :datetime
    add_column    :profiles, :address1, :string
    add_column    :profiles, :city, :string
    add_column    :profiles, :state, :string
    add_column    :profiles, :zip, :integer
    add_column    :profiles, :experience, :text
    add_index :profiles, [:id,:type]
  end
end
