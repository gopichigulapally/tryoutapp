class AddAddress2ToProfiles < ActiveRecord::Migration
  def change
    add_column    :profiles, :address2, :string
  end
end
