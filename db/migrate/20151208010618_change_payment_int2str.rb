class ChangePaymentInt2str < ActiveRecord::Migration
  def change
    remove_column :player_tryouts, :payment, :integer
    add_column    :player_tryouts, :payment, :string
  end
end
