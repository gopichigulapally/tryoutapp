class Player < Profile
  has_many :player_tryouts#, dependent: :destroy
  has_many :guardianships
end
