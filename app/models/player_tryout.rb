class PlayerTryout < ActiveRecord::Base
  belongs_to :player
  belongs_to :tryout
  has_many   :evaluations
  has_one    :payment_notification
end
#serialize :params, Hash
def paypal_url(pt_id,payment_amt)
  values = {
  	:business => 'aesotericon-facilitator@gmail.com',
    :cmd => '_xclick',
    :amount => payment_amt,
    :item_name => 'Soccer Tryout Registration',
    :return => 'http://cryptic-mesa-7823.herokuapp.com/players',
    :cancel_return => 'http://cryptic-mesa-7823.herokuapp.com/',
    :custom => pt_id,
    :notify_url => 'http://cryptic-mesa-7823.herokuapp.com/payment_notifications'
  }
  "https://www.sandbox.paypal.com/cgi-bin/webscr?" + values.to_query
end
