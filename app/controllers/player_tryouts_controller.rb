class PlayerTryoutsController < ApplicationController
  before_action :set_player_tryout, only: [:show, :edit, :update, :destroy]

  # GET /player_tryouts
  # GET /player_tryouts.json
  def index
    @player_tryouts = PlayerTryout.all
  end

  # GET /player_tryouts/1
  # GET /player_tryouts/1.json
  def show
  end

  # GET /player_tryouts/new
  def new
    @player_tryout = PlayerTryout.new
  end

  # GET /player_tryouts/1/edit
  def edit
  end

  # POST /player_tryouts
  # POST /player_tryouts.json
  def create


    @tryout = Tryout.find(params[:tryout_id])
    @player = Player.find(params[:player_id])

    if PlayerTryout.exists?(:tryout_id => @tryout.id, :player_id => @player.id)
    #dont create a new player tryout
   # redirect_to new_tryout_player_path(@tryout.id), notice: 'Player tryout exists.'
    redirect_to tryouts_path, notice: 'Player tryout exists.'
    else
    @player_tryout = @tryout.player_tryouts.create(:player_id => @player.id)

    respond_to do |format|
      if @player_tryout.save

        #this should go in update
        #@coaches = Coach.all
        #@coaches.each do |coach|
        #@evaluation = coach.evaluations.create(player_tryout_id: @player_tryout.id )
       # end


        format.html { redirect_to paypal_url(@player_tryout.id, @tryout.fee), notice: 'Player tryout was successfully created.' }

        #new_tryout_player_path(@tryout.id), notice: 'Player tryout was successfully created.' }
        #format.html { redirect_to new_player_path(tryout_id: @tryout.id), notice: 'Player tryout was successfully created.' }
        format.json { render :show, status: :created, location: @player_tryout }
      else
        format.html { render :new }
        format.json { render json: @player_tryout.errors, status: :unprocessable_entity }
      end
    end
    end
  end

  # PATCH/PUT /player_tryouts/1
  # PATCH/PUT /player_tryouts/1.json
  def update
    respond_to do |format|
      if @player_tryout.update(player_tryout_params)
        format.html { redirect_to @player_tryout, notice: 'Player tryout was successfully updated.' }
        format.json { render :show, status: :ok, location: @player_tryout }
      else
        format.html { render :edit }
        format.json { render json: @player_tryout.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /player_tryouts/1
  # DELETE /player_tryouts/1.json
  def destroy
    @player_tryout.destroy
    respond_to do |format|
      format.html { redirect_to player_tryouts_url, notice: 'Player tryout was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_player_tryout
      @player_tryout = PlayerTryout.find(params[:id])
    end


    # Never trust parameters from the scary internet, only allow the white list through.
    def player_tryout_params
      params.require(:player_tryout).permit(:player_id, :tryout_id, :payment)
    end
end
