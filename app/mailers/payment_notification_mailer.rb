require "mandrill"

class PaymentNotificationMailer < ActionMailer::Base

	default(
    	from: 'coach_justin@rushnm.com',
    	reply_to: 'coach_justin@rushnm.com'
  	)
  def notification_email(cur_player, cur_tryout, t_num)
  	@cur_player = cur_player
  	@cur_tryout = cur_tryout
  	@t_num = t_num
  	mail(to: @cur_player.email, subject: 'Your payment for Rush tryouts')
  end
end