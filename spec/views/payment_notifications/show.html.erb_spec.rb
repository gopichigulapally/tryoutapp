require 'rails_helper'

RSpec.describe "payment_notifications/show", type: :view do
  before(:each) do
    @payment_notification = assign(:payment_notification, PaymentNotification.create!(
      :params => "MyText",
      :composite_id => "Composite",
      :status => "Status",
      :transaction_id => "Transaction"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/Composite/)
    expect(rendered).to match(/Status/)
    expect(rendered).to match(/Transaction/)
  end
end
